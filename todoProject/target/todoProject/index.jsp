<%
	String contextPath = request.getContextPath();
	String title = "JSP application";
	String message = "To Do list App";
%>
<%!
	public String getDate(){
		return java.util.Calendar.getInstance().getTime().toString();
	}

%>
<html>
<title><%= title %></title>
</head>
<body>
<h2><%= message %></h2>
<h3>Examples: </h3>
<br/>
<ul>
	<li>-------------------------------</li>
	<li><a href="<%= contextPath %>/views/addActivity.jsp">Add New Activity</a></li>
	<li><a href="<%= contextPath %>/views/doneActivities.jsp">Done Activities</a></li>
	<li><a href="<%= contextPath %>/views/pendingActivities.jsp">Pending Activities</a></li>
</ul>
<br/>
	<%= getDate() %>
</body>
</html>
