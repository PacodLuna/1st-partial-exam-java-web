<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.softtek.academy.javaweb.dao.ToDoDAO"%>
<%@ page import="com.softtek.academy.javaweb.beans.ActivityBean"%>
<%@ page import="java.util.List"%>

<%
	List<ActivityBean> list = ToDoDAO.getUnDone();
	request.setAttribute("list", list);
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<th>ID</th>
			<th>List</th>
			<th></th>
		</tr>
		<c:forEach var="activity" items="${list}">
			<tr>
				<td><c:out value="${activity.getId()}" /></td>
				<td><c:out value="${activity.getList()}" /></td>
				<td>
					<form action="/todoProject/StatusControllerServlet" method="GET">
						<input type="hidden" value="${activity.getId()}" name="id">
						<input type="submit" value="Done">
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>

	<br />
	<a href="<%=contextPath%>">Home</a>
</body>
</html>