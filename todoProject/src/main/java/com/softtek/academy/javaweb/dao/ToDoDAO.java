package com.softtek.academy.javaweb.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.softtek.academy.javaweb.beans.ActivityBean;
import com.softtek.academy.javaweb.service.ToDoService;

public class ToDoDAO {

	public static boolean addActivity(ActivityBean activity) {
		return ToDoService.insertActivity(activity);
	}
	
	public static void changeStatus(int id) {
		ToDoService.changeSatus(id);
	}
	
	public static List<ActivityBean> getDone(){
		List<ActivityBean> list = new ArrayList<>();
		ResultSet rs = ToDoService.getAll();
		try {
			while(rs.next()) {
				ActivityBean activity = new ActivityBean(
						rs.getInt("id"), rs.getString("list"), rs.getBoolean("is_done"));
				if(activity.is_done()) {
					list.add(activity);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static List<ActivityBean> getUnDone(){
		List<ActivityBean> list = new ArrayList<>();
		ResultSet rs = ToDoService.getAll();
		try {
			while(rs.next()) {
				ActivityBean activity = new ActivityBean(
						rs.getInt("id"), rs.getString("list"), rs.getBoolean("is_done"));
				if(!activity.is_done()) {
					list.add(activity);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
}
