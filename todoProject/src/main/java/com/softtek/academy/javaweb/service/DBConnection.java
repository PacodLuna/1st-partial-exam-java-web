package com.softtek.academy.javaweb.service;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	private static Connection conn = null;

	public DBConnection() {
	}

	public static Connection getConnection() {
		if (conn == null) {
			final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
			final String DB_URL = "jdbc:mysql://localhost:3306/sesion3?"
					+ "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			final String USER = "root";
			final String PASS = "1234";
			Connection con = null;
			try {
				Class.forName(JDBC_DRIVER);
				con = DriverManager.getConnection(DB_URL, USER, PASS);
			} catch (Exception e) {
				System.out.println(e);
			}
			conn = con;
		}
		return conn;
	}
}