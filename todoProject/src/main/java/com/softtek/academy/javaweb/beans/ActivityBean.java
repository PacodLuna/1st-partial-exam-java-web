package com.softtek.academy.javaweb.beans;

public class ActivityBean {
	
	private int id;
	private String list;
	private boolean is_done;
	
	public ActivityBean() {
		super();
	}
	
	public ActivityBean(int id, String list, boolean is_done) {
		super();
		this.id = id;
		this.list = list;
		this.is_done = is_done;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public boolean is_done() {
		return is_done;
	}
	public void setIs_done(boolean is_done) {
		this.is_done = is_done;
	}

	@Override
	public String toString() {
		return "ActivityBean [id=" + id + ", list=" + list + ", is_done=" + is_done + "]";
	}

	
}
