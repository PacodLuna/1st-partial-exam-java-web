package com.softtek.academy.javaweb.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.beans.ActivityBean;
import com.softtek.academy.javaweb.dao.ToDoDAO;
import com.softtek.academy.javaweb.service.ToDoService;

/**
 * Servlet implementation class ServletController
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
        response.setContentType("text/html");
        String activity = request.getParameter("activity");
        ActivityBean bean = new ActivityBean();
        bean.setList(activity);
        if(ToDoDAO.addActivity(bean)) {
        	RequestDispatcher rd = request.getRequestDispatcher("/views/addActivity.jsp");
        	rd.forward(request, response);
        } else {
        	RequestDispatcher rd = request.getRequestDispatcher("/views/addActivity.jsp");
        	rd.forward(request, response);
        	System.out.println("Error :C");
        }
	}

}
