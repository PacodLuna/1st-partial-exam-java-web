package com.softtek.academy.javaweb.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.softtek.academy.javaweb.beans.ActivityBean;

public class ToDoService {
	
	public ToDoService() {}
	
	public static boolean insertActivity(ActivityBean activity) {
		String query = "INSERT INTO to_do_list (list) values(?);";
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, activity.getList());
			ps.execute();
		}catch(Exception e) {
            System.out.println(e);
            return false;
        }
		return true;
	}
	
	public static ResultSet getAll() {
		String query = "SELECT * FROM sesion3.to_do_list;";
		ResultSet rs = null;
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
		}catch(Exception e) {
            System.out.println(e);
            return rs;
        }
		return rs;
	}
	
	public static void changeSatus(int id) {
		String query = "UPDATE `sesion3`.`to_do_list` SET `is_done` = '1' WHERE (`id` = ?);";
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, id);
			ps.execute();
		}catch(Exception e) {
            System.out.println(e);
        }
	}
	
}
