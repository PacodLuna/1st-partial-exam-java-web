<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.softtek.academy.javaweb.dao.ToDoDAO"%>
<%@ page import="com.softtek.academy.javaweb.beans.ActivityBean"%>
<%@ page import="java.util.List"%>

<%
	List<ActivityBean> list = ToDoDAO.getDone();
	request.setAttribute("list", list);
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<th>ID</th>
			<th>List</th>
		</tr>
		<c:forEach var="activity" items="${list}">
			<tr>
				<td><c:out value="${activity.getId()}" /></td>
				<td><c:out value="${activity.getList()}" /></td>
			</tr>
		</c:forEach>
	</table>

	<br />
	<a href="<%=contextPath%>">Home</a>
</body>
</html>